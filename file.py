import argparse
import os
import sys
import pytesseract
import matplotlib.pyplot as plt
import cv2
import easyocr
from pylab import rcParams
#from IPython.display import Image
import requests
from io import BytesIO
import cv2
import numpy as np
import os
from PIL import Image
import argparse

EXPORT_MODEL_VERSION = 1
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

#pytesseract.pytesseract.tesseract_cmd = r"C:\Users\upai\PycharmProjects\generic-OCR\venv\Lib\site-packages\tesseract.exe"
rcParams['figure.figsize'] = 8, 16
reader = easyocr.Reader(['en', 'hi'])

parser = argparse.ArgumentParser(
    description="Script to process "
    "Images/Videos with different "
    
)
parser.add_argument("-i", dest="image_path", default=None, help="Image " "path")
results = parser.parse_args()

if results.image_path is None:
    raise ValueError("Image path not provided")

if results.image_path:
    filename = results.image_path
    output = reader.readtext(filename, add_margin=0.55, width_ths=0.7, link_threshold=0.8, decoder='beamsearch',
                             detail=0, paragraph=True)

print(output)
